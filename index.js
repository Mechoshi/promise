class MyPromise {
    constructor(fn) {
        this.fn = fn;
        this.value = undefined;
        this.error = undefined;
        this.status = 'pending';
        this.callback = null;
        this.errorCallback = null;
    }

    then(cb) {
        this.callback = cb;
        let newPromise = new MyPromise();

        this.resolve = value => {
            this.status = 'resolved';
            this.value = value;
            const res = this.callback(this.value);

            if (res instanceof MyPromise) {
                res.then(val => newPromise.resolve(val))
                    .catch(error => newPromise.reject(error))
            } else {
                newPromise.resolve(res);
            }
        };

        this.reject = error => {
            this.status = 'rejected';
            this.error = error;
            newPromise.status = 'rejected';

            if (newPromise.errorCallback) {
                newPromise.errorCallback(error);
            } else {
                newPromise.reject(error);
            }
        };

        this.fn && this.fn(this.resolve, this.reject);

        return newPromise;
    }

    catch(cb) {
        this.errorCallback = cb;
        this.reject = error => {
            this.status = 'rejected';
            this.error = error;
            this.errorCallback(this.error);
        };
    }

    reject() {
    }

    resolve() {
    }
}

const request = (id) => {
    return new MyPromise((resolve, reject) => {
        setTimeout(() => {
            if (id <= 100) {
                resolve(id);
            } else {
                console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  rejecting');
                reject('Id higher than 100');
            }
        }, 3000);
    });
};

const p1 = request(15);


p1.then((res) => {
    console.log(`Then 1: ${res}`);
    return 121
}).then((res) => {
    console.log(`Then 2: ${res}`);
    return request(16);
}).then((res) => {
    console.log(`Then 3: ${res}`);
    return request(171);
}).then((res) => {
    console.log(`Then 4: ${res}`);
    return request(18);
}).catch((error) => {
    console.log(`Catch: ${error}`);
});

console.log('MyPromise is non blocking!!!');

const asyncFn = async () => {
    const res = await request(1);
    console.log(`MyPromise is works with async/await: ${res}`)
};

asyncFn();